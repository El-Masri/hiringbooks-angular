(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('mainSearch', mainSearch);

  /** @ngInject */
  function mainSearch() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/mainsearch/mainsearch.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: MainSearchController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function MainSearchController($scope, $rootScope, $state, $location, $http, Search, Goodreads) {
      var scope = this;
      scope.global = $rootScope;
      scope.search = "";
      scope.selectedTab = 0;
      scope.searchResults = [];
      scope.global.keyword = "";

      scope.global.search = function(keyword) {
        scope.searchResults = [];
        Search.get("?keyword=" + keyword).then(function(data) {
          if (data.status !== false) {
            scope.searchResults = data;
          }
        });
        Goodreads.get(keyword, function(data){
          scope.searchResults.goodreads = data;
          Goodreads.set(scope.searchResults.goodreads);
        });
      };

      scope.global.getSearch = function($event) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
          $state.go($rootScope.guest ? 'guestsearch' : 'search', {
            keyword: $event.target.value
          });
          if ($event.target.value.length > 0) {
            scope.global.search($event.target.value.length);
          }
        }
      };

      scope.searchForKeyword = function() {
        if ($location.search().keyword && $location.search().keyword.length > 0) {
          scope.global.keyword = $location.search().keyword;
          scope.global.search($location.search().keyword);
        }
      };

      scope.navigateToUser = function(user) {
        $state.go('user', {user_id: user.id});
      };

      scope.searchForKeyword();
      $rootScope.$on('$locationChangeSuccess', scope.searchForKeyword);
    }
  }

})();