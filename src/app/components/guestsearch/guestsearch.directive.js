(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('guestSearch', guestSearch);

  /** @ngInject */
  function guestSearch() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/guestsearch/guestsearch.html',
      scope: {
        // user: '@',
        // addchatwindow: '='
      },
      controller: GuestSearchController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function GuestSearchController($scope, $rootScope, $state, $location, $http, Search, Goodreads) {
      var scope = this;
      scope.global = $rootScope;
      scope.search = "";
      scope.selectedTab = 0;
      scope.searchResults = [];
      scope.global.keyword = "";

      scope.global.search = function(keyword) {
        scope.searchResults = [];
        Search.get("?keyword=" + keyword).then(function(data) {
          if (data.status !== false) {
            scope.searchResults = data;
          }
        });
        Goodreads.get(keyword, function(data){
          scope.searchResults.goodreads = data;
          Goodreads.set(scope.searchResults.goodreads);
        });
      };

      scope.searchForKeyword = function() {
        if ($location.search().keyword && $location.search().keyword.length > 0) {
          scope.global.keyword = $location.search().keyword;
          scope.global.search($location.search().keyword);
        }
      };

      scope.navigateToUser = function(user) {
        $state.go('guestuser', {user_id: user.id});
      };

      scope.searchForKeyword();
      $rootScope.$on('$locationChangeSuccess', scope.searchForKeyword);
    }
  }

})();