(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .controller('BookModalController', BookModalController);

  /** @ngInject */
  function BookModalController($scope, price, $uibModalInstance) {
    $scope.price = price;

    $scope.ok = function() {
      if ($scope.price.length === 0) {
        $scope.price = 0;
      }
      $uibModalInstance.close($scope.price);
    };

    $scope.okPress = function($event) {
      if ($event.keyCode == 13) {
        if ($scope.price.length === 0) {
          $scope.price = 0;
        }
        $uibModalInstance.close($scope.price);
      }
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    };
  }

})();