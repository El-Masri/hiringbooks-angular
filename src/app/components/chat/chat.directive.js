(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('chat', chat);

  /** @ngInject */
  function chat() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/chat/chat.html',
      scope: {
        user: '=',
        index: '=',
        closechatwindow: '='
      },
      controller: ChatController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function ChatController($rootScope, $scope, Messages, socketpath) {
      var scope = this;
      var socket = io(socketpath);

      scope.typing = false;
      scope.newMessage = "";
      scope.global = $rootScope;
      scope.user.minimized = false;
      scope.user.message_loading = false;
      scope.messages = scope.user.messages ? scope.user.messages : [];

      socket.on('user_typing', function(data) {
        if (data.source == scope.user.id && data.target == $rootScope.user.id) {
          scope.user.typing = true;
          $scope.$apply();
        }
      });

      socket.on('user_stop_typing', function(data) {
        if (data.source == scope.user.id && data.target == $rootScope.user.id) {
          scope.user.typing = false;
          $scope.$apply();
        }
      });

      socket.on('private message', function(message) {
        if (message.user !== $rootScope.user.id) {
          message.time = moment(message.time).format('h:mm a');
          scope.messages.push(message);
          $scope.$apply();
          Messages.set_seen({
            user_id: $rootScope.user.id,
            target_id: scope.user.id
          }).then(function(data) {
            console.log(data);
          });
        }
      });

      scope.preventNewLine = function($event) {
        if ($event.keyCode == 13) {
          $event.preventDefault();
        }
      };

      scope.sendMessage = function($event) {
        if ($event.target.value.length > 0) {
          socket.emit('typing', {
            source: $rootScope.user.id,
            target: scope.user.id
          });
        } else if ($event.target.value.length === 0) {
          socket.emit('stop typing', {
            source: $rootScope.user.id,
            target: scope.user.id
          });
        }

        if ($event.keyCode == 13) {
          $event.preventDefault();
          if ($event.target.value.length > 0) {
            scope.messages.push({
              mine: true,
              message: $event.target.value,
              time: moment().format('h:mm a')
            });
            Messages.post({
              user_id: $rootScope.user.id,
              target_id: scope.user.id,
              message: $event.target.value,
              seen: false,
              timestamp: moment()
            }).then(function(data) {
              console.log(data);
            });
            socket.emit('new message', {
              message: {
                user: $rootScope.user.id,
                target_user_id: scope.user.id,
                mine: false,
                message: $event.target.value,
                time: moment()
              }
            });
            $event.target.value = "";
          }
        }
      };

      scope.fetchMessages = function() {
        scope.user.message_loading = true;
        Messages.get("?user_id=" + $rootScope.user.id + "&target_id=" + scope.user.id + "&offset=" + scope.user.message_offset).then(function(data) {
          scope.user.message_loading = false;
          if (data.length > 0) {
            scope.user.message_offset += data.length;
            _.each(data, function(message) {
              message.mine = message.user_id == $rootScope.user.id;
              message.time = moment(message.timestamp).format('h:mm a');
              scope.user.messages.unshift(message);
            });
          }
        });
      };
    }
  }

})();