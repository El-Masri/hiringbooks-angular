(function() {
  'use strict';

  angular
      .module('hiringbooksAngular')
      .service('users', users);

  /** @ngInject */
  function users(Restangular) {
    var service = Restangular.all("users");
    service.addRestangularMethod('current_user', 'get', 'current_user');
    service.addRestangularMethod('suspend_user', 'post', 'suspend_user');
    service.addRestangularMethod('delete_user', 'post', 'delete_user');
    service.addRestangularMethod('rate_user', 'post', 'rate_user');
    return service;
  }

})();
