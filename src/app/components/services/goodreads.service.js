(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .factory('Goodreads', Goodreads);

  /** @ngInject */
  function Goodreads($http, $q, $rootScope) {
    var canceler = $q.defer();
    return {
      get: function(keyword, callback) {
        var url = "http://www.goodreads.com/search/index.xml?key=3O7gQmdLlhZPzVH9qgXlZQ&q=";
        url = url + encodeURIComponent(keyword);
        canceler.resolve();
        canceler = $q.defer();
        $http.get("http://query.yahooapis.com/v1/public/yql", {
            timeout: canceler.promise,
            params: {
              timeout: canceler.promise,
              q: "select * from xml where url=\"" + url + "\"",
              format: "json"
            }
          })
          .success(function(template, status, headers, config) {
            callback(template.query.results.GoodreadsResponse.search.results.work);
          });
      },
      set: function(searchedBooksArray) {
       var resultIsArray = _.isArray(searchedBooksArray);
       if (!resultIsArray) {
         searchedBooksArray = [searchedBooksArray];
       }
       _.each(searchedBooksArray, function(book, index) {
         searchedBooksArray[index].submitLoading = false;
         searchedBooksArray[index].commentMode = false;
         var bookIndex = _.findIndex($rootScope.user.books, function(user_book) {
           return user_book.goodreads_id == book.best_book.id.content;
         });
         var requestIndex = _.findIndex($rootScope.user.requests, function(user_book) {
           return user_book.goodreads_id == book.best_book.id.content;
         });
         if (bookIndex > -1 || requestIndex > -1) {
           searchedBooksArray[index].mine = true;
         } else {
           searchedBooksArray[index].mine = false;
         }
       });
      }
    };
  }
})();