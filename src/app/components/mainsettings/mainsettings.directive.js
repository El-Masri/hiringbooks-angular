(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .directive('mainSettings', mainSettings);

  /** @ngInject */
  function mainSettings() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/mainsettings/mainsettings.html',
      scope: {
        user: '@',
        addchatwindow: '='
      },
      controller: MainSettingsController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function MainSettingsController($scope, $rootScope, $uibModal, users) {
      var scope = this;

      scope.picture = {};
      scope.imgParser = function(file, base64_object) {
        scope.openModal("data:image/jpeg;base64," + base64_object.base64);
      };

      scope.openModal = function(pictureToCrop) {
        var modalInstance = $uibModal.open({
          animation: true,
          controller: 'ImageModalController',
          templateUrl: 'app/components/mainsettings/imageModal.html',
          resolve: {
            picture: function() {
              return pictureToCrop;
            }
          }
        });

        modalInstance.result.then(function(croppedPicture) {
          scope.global.user.img = croppedPicture;
        });
      };

      scope.global = $rootScope;
      scope.submitLoading = false;
      scope.passwordVisible = 'password';
      scope.submit = function() {
        scope.submitLoading = true;
        var update_data = {
          id: $rootScope.user.id,
          fullname: $rootScope.user.fullname,
          phone: $rootScope.user.phone,
          city: $rootScope.user.city,
          college: $rootScope.user.college,
          img: $rootScope.user.img.replace("data:image/png;base64,", ""),
          bio: $rootScope.user.bio
        };
        if ($rootScope.user.password.length > 0) {
          update_data.password = $rootScope.user.password;
        }
        users.post(update_data).then(function(responde) {
          $rootScope.user = responde;
          scope.submitLoading = false;
          $rootScope.$broadcast("user_updated");
        });
      };
    }
  }

})();