/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .filter('round', function() {
      return function(data) {
        return Math.round(parseFloat(data));
      };
    })
    .filter('relativeDate', function() {
      return function(date) {
        return moment(date).fromNow();
      };
    })
    .filter('rgbToHsl', function() {
      return function(rgb) {
        var r = rgb[0];
        r /= 255;
        var g = rgb[1];
        g /= 255;
        var b = rgb[2];
        b /= 255;
        var max = Math.max(r, g, b),
          min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if (max == min) {
          h = s = 0; // achromatic
        } else {
          var d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
          switch (max) {
            case r:
              h = (g - b) / d + (g < b ? 6 : 0);
              break;
            case g:
              h = (b - r) / d + 2;
              break;
            case b:
              h = (r - g) / d + 4;
              break;
          }
          h /= 6;
        }
        if (_.isArray(rgb)) {
          return [Math.floor(h * 360), Math.floor(s * 100) + "%", Math.floor(l * 100) - 40 + "%"];
        }
      };
    })
    .filter('range', function() {
      return function(n) {
        var res = [];
        for (var i = 0; i < Math.round(parseFloat(n)); i++) {
          res.push(i);
        }
        return res;
      };
    });
})();