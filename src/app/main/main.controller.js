(function() {
  'use strict';

  angular
    .module('hiringbooksAngular')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($rootScope, $scope, $state, $location, users, books, Requests, Messages, Notification, webNotification, socketpath) {
    var scope = this;
    var socket = io(socketpath);
    $rootScope.user = {};
    $rootScope.users = [];
    $rootScope.selectedUser = {};
    $rootScope.online_users = [];
    $rootScope.default_img = "assets/images/default-user.png";

    scope.update_online_users = function() {
      _.each($rootScope.users, function(user, index) {
        if ($rootScope.online_users.indexOf(user.id) > -1) {
          $rootScope.users[index].online = "online";
        } else {
          $rootScope.users[index].online = "offline";
        }
        books.get('?id=' + $rootScope.users[index].id).then(function(data) {
          if (data.status !== false) {
            $rootScope.users[index].books = data.books;
            $rootScope.users[index].books_comments = data.comments;
          } else {
            $rootScope.users[index].books = [];
            $rootScope.users[index].books_comments = [];
          }
        });
        Requests.get('?id=' + $rootScope.users[index].id).then(function(data) {
          if (data.status !== false) {
            $rootScope.users[index].requests = data.requests;
            $rootScope.users[index].requests_comments = data.comments;
          } else {
            $rootScope.users[index].requests = [];
            $rootScope.users[index].requests_comments = [];
          }
        });
      });
      if (!$rootScope.$$phase) {
        $rootScope.$broadcast("users_updated");
      }
    };

    socket.on('message', function(message) {
      if (message.title == "onlineUsers" && _.isArray(message.data)) {
        users.getList().then(function(data) {
          $rootScope.users = data;
          $rootScope.online_users = message.data;
          scope.update_online_users();
          scope.getCurrentUser();
        });
      } else if (message.title == "chatMessage" && _.isObject(message.data)) {

      } else if (message.title == "socketid" && _.isString(message.data)) {
        $rootScope.socketID = message.data;
      }
    });

    scope.getCurrentUser = function() {
      users.current_user().then(function(data) {
        $rootScope.user = data;
        $rootScope.user.password = "";
        $rootScope.user.online = "online";
        $rootScope.user.messages_unseen = [];
        $rootScope.user.notifications_unseen = 0;
        _.each(data.notifications, function(notification) {
          if (notification.seen === 0) {
            $rootScope.user.notifications_unseen++;
          }
        });
        var userIndexs = [];
        _.each(data.messages, function(message) {
          if (message.seen === 0) {
            _.each($rootScope.users, function(user, index) {
              if (user.id == message.user_id) {
                if (!$rootScope.users[index].unseen_messages) {
                  $rootScope.users[index].unseen_messages = 0;
                }
                userIndexs.push(index);
              }
            });
          }
        });
        _.each($rootScope.users, function(user) {
          var messageIndex = _.findLastIndex(userIndexs, function(index) {
            return user.id == $rootScope.users[index].id;
          });
          if (messageIndex >= 0) {
            $rootScope.user.messages_unseen.push(data.messages[messageIndex]);
          }
        });
        books.get('?id=' + $rootScope.user.id).then(function(data) {
          if (data.status !== false) {
            $rootScope.user.books = data.books;
            $rootScope.user.books_comments = data.comments;
          } else {
            $rootScope.user.books = [];
            $rootScope.user.books_comments = [];
          }
          if (!$rootScope.$$phase) {
            $rootScope.$broadcast("user_updated");
          }
        });
        Requests.get('?id=' + $rootScope.user.id).then(function(data) {
          if (data.status !== false) {
            $rootScope.user.requests = data.requests;
            $rootScope.user.requests_comments = data.requests_comments;
          } else {
            $rootScope.user.requests = [];
            $rootScope.user.requests_comments = [];
          }
          if (!$rootScope.$$phase) {
            $rootScope.$broadcast("user_updated");
          }
        });
        scope.updateState();
        socket.emit('add user', $rootScope.user.id);
        if ($state.current.name == "home") {
          $state.go("user", {
            'user_id': $rootScope.user.id
          });
        }
      }, function errorCallback(error) {
        if (error.status == 404) {
          window.location.assign("/auth");
        }
      });
    };

    socket.on('user joined', function(data) {
      $rootScope.online_users = data.onlineUsers;
      if ($rootScope.users && $rootScope.users.length > 0) {
        scope.update_online_users();
      }
    });

    scope.updateState = function() {
      if ($state.params.user_id && $state.params.user_id != $rootScope.user.id) {
        var userIndex = _.findIndex($rootScope.users, function(user) {
          return user.id == $state.params.user_id;
        });
        $rootScope.selectedUserIsMe = false;
        $rootScope.selectedUser = $rootScope.users[userIndex];
      } else {
        $rootScope.selectedUserIsMe = true;
        $rootScope.selectedUser = $rootScope.user;
      }
      $rootScope.gender = $rootScope.selectedUser.gender == 1 ? "her" : "his";
      $rootScope.state = $state.current.name;
    };

    $rootScope.$on('$routeChangeStart', scope.updateState);
    scope.global = $rootScope;

    scope.addChatWindow = function(user, index) {
      var subscribe = {
        user_id: user.id,
        user_index: index,
        current_user: $rootScope.user.id,
        current_index: _.findIndex($rootScope.users, function(u) {
          return u.id == $rootScope.user.id;
        })
      };
      user.messages = [];
      user.message_offset = 0;
      Messages.get("?user_id=" + $rootScope.user.id + "&target_id=" + user.id + "&offset=" + user.message_offset).then(function(data) {
        if (_.isArray(data) && data.length > 0) {
          _.each(data, function(message) {
            message.mine = message.user_id == $rootScope.user.id;
            message.time = moment(message.timestamp).format('h:mm a');
            user.messages.push(message);
          });
          user.message_offset += data.length;
        }
        if ($rootScope.chat_windows.indexOf(user) < 0) {
          $rootScope.chat_windows.push(user);
        }
      });
      Messages.set_seen({
        user_id: $rootScope.user.id,
        target_id: user.id
      }).then(function(data) {
        console.log(data);
      });
      socket.emit('subscribe', subscribe);
    };

    socket.on('room_id', function(data) {
      console.log($rootScope.user.id, data.user_id);
      if (_.findIndex($rootScope.chat_windows, $rootScope.users[data.current_index]) == -1 && $rootScope.user.id == data.user_id) {
        console.log($rootScope.users[data.current_index]);
        $rootScope.chat_windows.push($rootScope.users[data.current_index]);
        $rootScope.$apply();
      }
    });

    $scope.navigateToLatestNotification = function() {
      $rootScope.user.notifications[0].navigate();
    };

    socket.on('new notification', function(data) {
      var notification = JSON.parse(data.notification);
      var notification_user = $rootScope.users[_.findIndex($rootScope.users, function(user) {
        return user.id == notification.user_id;
      })];

      notification.navigate = function() {
        $state.go(notification.topic_type, {
          'user_id': notification.target_user_id,
          'book_id': notification.topic_id
        });
      };

      if (notification.user_id !== $rootScope.user.id && notification.notification.length !== 0) {
        $rootScope.user.notifications_unseen++;
        $rootScope.user.notifications.unshift(notification);
        Notification.success({
          message: notification.name + " " + notification.notification,
          templateUrl: "app/main/notification.html",
          scope: $scope
        });
        $rootScope.$apply();
        if (!$rootScope.focus) {
          webNotification.showNotification('Example Notification', {
            body: notification.name + " " + notification.notification,
            icon: notification_user.img,
            onClick: function onNotificationClicked() {
              notification.navigate();
            },
            autoClose: 4000
          }, function onShow(error, hide) {
            if (error) {
              console.log('Unable to show notification: ' + error.message);
            } else {
              console.log('Notification Shown.');
              setTimeout(function hideNotification() {
                hide();
              }, 5000);
            }
          });
        }
      }
    });

    scope.closeChatWindow = function($index) {
      $rootScope.chat_windows.splice($index, 1);
    };
  }
})();